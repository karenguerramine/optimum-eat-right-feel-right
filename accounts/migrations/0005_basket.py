<<<<<<< HEAD
# Generated by Django 4.1.1 on 2022-09-21 19:39

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ("recipes", "0002_rename_recipes_recipe_rename_steps_step"),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("accounts", "0004_tsubscription_description_and_more"),
    ]

    operations = [
        migrations.CreateModel(
            name="Basket",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "recipe_item",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.PROTECT, to="recipes.recipe"
                    ),
                ),
                (
                    "user",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="basket",
                        to=settings.AUTH_USER_MODEL,
                    ),
                ),
            ],
        ),
    ]
=======
# Generated by Django 4.1.1 on 2022-09-21 19:39

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ("recipes", "0002_rename_recipes_recipe_rename_steps_step"),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("accounts", "0004_tsubscription_description_and_more"),
    ]

    operations = [
        migrations.CreateModel(
            name="Basket",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "recipe_item",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.PROTECT, to="recipes.recipe"
                    ),
                ),
                (
                    "user",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="basket",
                        to=settings.AUTH_USER_MODEL,
                    ),
                ),
            ],
        ),
    ]
>>>>>>> 28538822537e17c6b29dc5759d80c92b4d0e425f
