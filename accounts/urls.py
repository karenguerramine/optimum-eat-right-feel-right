from django.urls import path

from django.contrib.auth import views as auth_views
from accounts.views import signup, SubscriptionListView, SubscriptionDetailView, BasketListView

urlpatterns = [
    path("login/", auth_views.LoginView.as_view(), name = "login"),
    path("logout/", auth_views.LogoutView.as_view(), name = "logout"),
    path("signup/", signup, name="signup"),
    path("price/", SubscriptionListView.as_view(), name = "price"),
    path("<int:pk>/", SubscriptionDetailView.as_view(), name = "price_detail"),
    path("basket/", BasketListView.as_view(), name = "basket")
]
