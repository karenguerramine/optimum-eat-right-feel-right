from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect
from django.contrib.auth import login
from accounts.models import Tsubscription, Basket, SubPlan, SubPlanFeature
from django.forms import BaseModelFormSet
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.edit import CreateView

def signup(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            username = request.POST.get("username")
            password = request.POST.get("password1")
            # firstname = request.POST.get("firstname")
            # lastname = request.POST.get("lastname")
            # age = request.POST.get("age")
            # gender = request.POST.get('gender')
            # address = request.POST.get("address")
            user = User.objects.create_user(
                username=username, password=password
            )
            # , firstname = firstname, lastname = lastname, age = age, gender = gender, address = address
            user.save()
            login(request, user)
            return redirect("home")
    else:
        form = UserCreationForm()
    context = {"form":form}
    return render(request, "registration/signup.html", context)



class SubscriptionListView(ListView):
    model = SubPlan
    template_name = "accounts/price.html"

    # def get_user_tsubscription(self):
    #     user_tsubscription_qs=Usersubscription.objects.filter(user=self.request.user)
    #     if user_sub_qs.exists():
    #         return user_sub_qs.first()
    #     return None

    # def get_context_data(self, *args,**kwargs):
    #     context = super().get_context_data(**kwargs)
    #     current_sub = self.get_subscription(self.request)
    #     context["current_sub"] = str(current_sub.tsubscription)
    #     return context

class SubscriptionCreate(CreateView):
    model = Tsubscription
    template_name = "accounts/create.html"
    fields = ["subscription"]

    def form_invalid(self, form):
        plan = form.save(commit=False)
        plan.owner=self.request.user
        plan.save()
        form.save_m2m()
        return redirect("home", pk=plan.id)
class SubscriptionDetailView(DetailView):
    model = SubPlan
    template_name = "accounts/detail.html"

class SubplanCheckout(ListView):
    model = SubPlan

class BasketListView(LoginRequiredMixin, ListView):
    model = Basket
    template_name = "basket/list.html"

    def get_queryset(self):
        return Basket.objects.filter(user=self.request.user)
