from django.contrib import admin
from accounts.models import (
    Subscription,
    Tsubscription,
    # Usersubscription,
    # Subinfo,
    SubPlan,
    SubPlanFeature

)
# Register your models here.
admin.site.register(Subscription)
admin.site.register(Tsubscription)
# admin.site.register(Usersubscription),
# admin.site.register(Subinfo)
admin.site.register(SubPlan)
admin.site.register(SubPlanFeature)
