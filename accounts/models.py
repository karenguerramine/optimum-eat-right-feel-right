from django.db import models
from django.contrib.auth.models import User
from django.conf import settings

# Create your models here.
class Subscription(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField()
    price = models.IntegerField()
    tax = models.FloatField() #find API for tax from each state
    total = models.FloatField()
    member = models.ForeignKey(User, related_name="members", on_delete=models.CASCADE)

SUBSCRIPTION_CHOICES = [
        ('Best', "i am the best plan $250"),
        ('Ehhh', "Do you really care about health? $150" ),
        ('Worst', "Yea you dont care $75")
    ]
class Tsubscription(models.Model):
    slug = models.SlugField(null=True, blank=True)
    subscription = models.CharField(max_length=50, choices=SUBSCRIPTION_CHOICES)
    price = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    # owner = models.ForeignKey(User, related_name="tsub", on_delete=models.CASCADE, null = True)

    def __str__(self):
        return self.subscription

class SubPlan(models.Model):
    title=models.CharField(max_length=150)
    price = models.IntegerField()
    highlight_status=models.BooleanField(default=False, null=True)

    def __str__(self):
        return self.title

class SubPlanFeature(models.Model):
    subplan = models.ForeignKey(SubPlan, related_name="subplan" ,on_delete=models.CASCADE)
    title= models.CharField(max_length=150)

    def __str__(self):
        return self.title




# class Usersubscription(models.Model):
#     user = models.OneToOneField(settings.AUTH_USER_MODEL, related_name="user_sub", on_delete=models.SET_NULL, null =True)
#     subscription = models.ForeignKey("Tsubscription", related_name='user_sub', on_delete=models.SET_NULL, null = True)

#     def __str__(self):
#         return self.user.username

# class Subinfo(models.Model):
#     subscription = models.ForeignKey("Usersubscription", related_name="subinfo", on_delete=models.CASCADE)
#     active = models.BooleanField(default=True)

#     def __str__(self):
#         return self.subscription.user.username



class Basket(models.Model):
    user = models.ForeignKey(User, related_name="basket", on_delete=models.CASCADE)
    recipe_item = models.ForeignKey("recipes.Recipe", on_delete=models.PROTECT)

    def __str__(self):
        return str(self.recipe_item)
