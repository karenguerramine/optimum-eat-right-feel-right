from django.urls import path

from recipes.views import (
    RecipeListView,
    RecipeDetailView,
    ProductListView
)

from django.views.generic.base import RedirectView

urlpatterns = [
    path("list/", RecipeListView.as_view(), name="recipe_list"),
    path("<str:pk>/", RecipeDetailView.as_view(), name = "recipe_detail"),
    path("", ProductListView.as_view(), name = "home")
]
