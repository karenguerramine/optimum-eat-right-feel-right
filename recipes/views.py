from django.shortcuts import render
from django.views.generic.list import ListView
from recipes.models import Recipe
from django.views.generic.detail import DetailView
from django.core.paginator import Paginator

# Create your views here.
class RecipeListView(ListView):
    model = Recipe
    template_name = "recipes/list.html"
    paginate_by = 2

class RecipeDetailView(DetailView):
    model = Recipe
    template_name = "recipes/detail.html"

class ProductListView(ListView):
    model = Recipe
    template_name = "home.html"
