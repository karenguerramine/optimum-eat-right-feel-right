from django.db import models
from django.conf import settings
# Create your models here.

USER_MODEL = settings.AUTH_USER_MODEL


class Recipe(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField()
    image = models.URLField(null=True, blank= True)
    servings = models.PositiveIntegerField(blank=True)

    def __str__(self):
        return self.name


class Ingredient(models.Model):
    name = models.CharField(max_length=50)
    measurement = models.ForeignKey("Measure", related_name="measurement", on_delete=models.PROTECT)
    amount = models.FloatField()
    recipe = models.ForeignKey("Recipe", related_name="ingredients", on_delete=models.CASCADE)

    def __str__(self):
        amount = str(self.amount)
        measurement = self.measurement.name
        name = self.name
        return amount + " " + measurement + " " + name

class Step(models.Model):
    recipe = models.ForeignKey("Recipe", related_name="steps", on_delete=models.CASCADE)
    directions = models.CharField(max_length=300)
    order = models.PositiveSmallIntegerField()

    def __str__(self):
        return f'Step {self.order}: {self.directions} - {self.recipe.name} recipe'


class Measure(models.Model):
    name = models.CharField(max_length=30, unique = True)
    abbreviation = models.CharField(max_length=10, unique=True)

    def __str__(self):
        return self.name
